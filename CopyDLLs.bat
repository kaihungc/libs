@REM Debug
if not exist ..\simulationandcontrolplaygroundcode\debug\AntTweakBar.dll 	echo f | xcopy /y AntTweakBar.dll ..\simulationandcontrolplaygroundcode\debug\AntTweakBar.dll 

if not exist ..\simulationandcontrolplaygroundcode\debug\glew32.dll		echo f | xcopy /y glew32.dll ..\simulationandcontrolplaygroundcode\debug\glew32.dll

if not exist ..\simulationandcontrolplaygroundcode\debug\dxl_x86_cpp.dll	echo f | xcopy /y dxl_x86_cpp.dll ..\simulationandcontrolplaygroundcode\debug\dxl_x86_cpp.dll 

@REM more directories for Martin...
if not exist ..\simulation_drz\debug\AntTweakBar.dll 				echo f | xcopy /y AntTweakBar.dll ..\simulation_drz\debug\AntTweakBar.dll 

if not exist ..\simulation_drz\debug\glew32.dll					echo f | xcopy /y glew32.dll ..\simulation_drz\debug\glew32.dll

if not exist ..\simulation_drz\debug\dxl_x86_cpp.dll				echo f | xcopy /y dxl_x86_cpp.dll ..\simulation_drz\debug\dxl_x86_cpp.dll 


if not exist ..\dr_simulation_control\debug\AntTweakBar.dll 				echo f | xcopy /y AntTweakBar.dll ..\dr_simulation_control\debug\AntTweakBar.dll 

if not exist ..\dr_simulation_control\debug\glew32.dll					echo f | xcopy /y glew32.dll ..\dr_simulation_control\debug\glew32.dll

if not exist ..\dr_simulation_control\debug\dxl_x86_cpp.dll				echo f | xcopy /y dxl_x86_cpp.dll ..\dr_simulation_control\debug\dxl_x86_cpp.dll 

@REM Release
if not exist ..\simulationandcontrolplaygroundcode\release\AntTweakBar.dll 	echo f | xcopy /y AntTweakBar.dll ..\simulationandcontrolplaygroundcode\release\AntTweakBar.dll 

if not exist ..\simulationandcontrolplaygroundcode\release\glew32.dll		echo f | xcopy /y glew32.dll ..\simulationandcontrolplaygroundcode\release\glew32.dll

@REM more directories for Martin...
if not exist ..\simulation_drz\release\AntTweakBar.dll 				echo f | xcopy /y AntTweakBar.dll ..\simulation_drz\release\AntTweakBar.dll 

if not exist ..\simulation_drz\release\glew32.dll				echo f | xcopy /y glew32.dll ..\simulation_drz\release\glew32.dll


@REM echo f | xcopy /y ..\dr_simulation_control\release\AikidoManaged.dll  ..\Unity Integration\Unity\Aikido Player\Assets\Plugins\AikidoManaged.dll 

if not exist ..\dr_simulation_control\release\AntTweakBar.dll 				echo f | xcopy /y AntTweakBar.dll ..\dr_simulation_control\release\AntTweakBar.dll 

if not exist ..\dr_simulation_control\release\glew32.dll				echo f | xcopy /y glew32.dll ..\dr_simulation_control\release\glew32.dll




@REM RelWithDebInfo
if not exist ..\simulationandcontrolplaygroundcode\RelWithDebInfo\AntTweakBar.dll 	echo f | xcopy /y AntTweakBar.dll ..\simulationandcontrolplaygroundcode\RelWithDebInfo\AntTweakBar.dll 

if not exist ..\simulationandcontrolplaygroundcode\RelWithDebInfo\glew32.dll			echo f | xcopy /y glew32.dll ..\simulationandcontrolplaygroundcode\RelWithDebInfo\glew32.dll

@pause
