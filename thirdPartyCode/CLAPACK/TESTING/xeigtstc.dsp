# Microsoft Developer Studio Project File - Name="xeigtstc" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=xeigtstc - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "xeigtstc.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "xeigtstc.mak" CFG="xeigtstc - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "xeigtstc - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "xeigtstc - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "xeigtstc - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Eig_Release"
# PROP Intermediate_Dir "Eig_Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 Release\tmglib.lib ..\Release\clapack.lib ..\blas\Release\blas.lib ..\f2clibs\Release\libF77.lib ..\f2clibs\Release\libI77.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386

!ELSEIF  "$(CFG)" == "xeigtstc - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "xeigtstc___Win32_Debug"
# PROP BASE Intermediate_Dir "xeigtstc___Win32_Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Eig_Debug"
# PROP Intermediate_Dir "Eig_Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 Debug\tmglib.lib ..\Debug\clapack.lib ..\blas\Debug\blas.lib ..\f2clibs\Debug\libF77.lib ..\f2clibs\Debug\libI77.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "xeigtstc - Win32 Release"
# Name "xeigtstc - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Group "aeigtst"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\Eig\alahdg.c
# End Source File
# Begin Source File

SOURCE=.\Eig\alareq.c
# End Source File
# Begin Source File

SOURCE=.\Eig\alasum.c
# End Source File
# Begin Source File

SOURCE=.\Eig\alasvm.c
# End Source File
# Begin Source File

SOURCE=.\EIG\cdrges.c
# End Source File
# Begin Source File

SOURCE=.\EIG\cdrgev.c
# End Source File
# Begin Source File

SOURCE=.\EIG\cdrgsx.c
# End Source File
# Begin Source File

SOURCE=.\EIG\cdrgvx.c
# End Source File
# Begin Source File

SOURCE=.\EIG\cget54.c
# End Source File
# Begin Source File

SOURCE=.\Eig\chkxer.c
# End Source File
# Begin Source File

SOURCE=.\EIG\cstt22.c
# End Source File
# Begin Source File

SOURCE=.\Eig\ilaenv.c
# End Source File
# Begin Source File

SOURCE=.\Eig\xerbla.c
# End Source File
# Begin Source File

SOURCE=.\Eig\xlaenv.c
# End Source File
# End Group
# Begin Group "ceigtst"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\Eig\cbdt01.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cbdt02.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cbdt03.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cchkbb.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cchkbd.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cchkbk.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cchkbl.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cchkec.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cchkee.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cchkgg.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cchkgk.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cchkgl.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cchkhb.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cchkhs.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cchkst.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cckglm.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cckgqr.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cckgsv.c
# End Source File
# Begin Source File

SOURCE=.\Eig\ccklse.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cdrvbd.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cdrves.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cdrvev.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cdrvgg.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cdrvsg.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cdrvst.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cdrvsx.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cdrvvx.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cerrbd.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cerrec.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cerred.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cerrgg.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cerrhs.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cerrst.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cget02.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cget10.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cget22.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cget23.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cget24.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cget35.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cget36.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cget37.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cget38.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cget51.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cget52.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cglmts.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cgqrts.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cgrqts.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cgsvts.c
# End Source File
# Begin Source File

SOURCE=.\Eig\chbt21.c
# End Source File
# Begin Source File

SOURCE=.\Eig\chet21.c
# End Source File
# Begin Source File

SOURCE=.\Eig\chet22.c
# End Source File
# Begin Source File

SOURCE=.\Eig\chpt21.c
# End Source File
# Begin Source File

SOURCE=.\Eig\chst01.c
# End Source File
# Begin Source File

SOURCE=.\MATGEN\clakf2.c
# End Source File
# Begin Source File

SOURCE=.\Eig\clarfy.c
# End Source File
# Begin Source File

SOURCE=.\Eig\clarhs.c
# End Source File
# Begin Source File

SOURCE=.\Eig\clatm4.c
# End Source File
# Begin Source File

SOURCE=.\MATGEN\clatm5.c
# End Source File
# Begin Source File

SOURCE=.\MATGEN\clatm6.c
# End Source File
# Begin Source File

SOURCE=.\EIG\clctes.c
# End Source File
# Begin Source File

SOURCE=.\EIG\clctsx.c
# End Source File
# Begin Source File

SOURCE=.\Eig\clsets.c
# End Source File
# Begin Source File

SOURCE=.\Eig\csbmv.c
# End Source File
# Begin Source File

SOURCE=.\Eig\csgt01.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cslect.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cstt21.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cunt01.c
# End Source File
# Begin Source File

SOURCE=.\Eig\cunt03.c
# End Source File
# End Group
# Begin Group "scigtst"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\Eig\slafts.c
# End Source File
# Begin Source File

SOURCE=.\Eig\slahd2.c
# End Source File
# Begin Source File

SOURCE=.\Eig\slasum.c
# End Source File
# Begin Source File

SOURCE=.\Eig\slatb9.c
# End Source File
# Begin Source File

SOURCE=.\Eig\sstech.c
# End Source File
# Begin Source File

SOURCE=.\Eig\sstect.c
# End Source File
# Begin Source File

SOURCE=.\Eig\ssvdch.c
# End Source File
# Begin Source File

SOURCE=.\Eig\ssvdct.c
# End Source File
# Begin Source File

SOURCE=.\Eig\ssxt1.c
# End Source File
# End Group
# End Group
# End Target
# End Project
