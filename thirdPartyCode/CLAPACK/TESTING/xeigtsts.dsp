# Microsoft Developer Studio Project File - Name="xeigtsts" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=xeigtsts - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "xeigtsts.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "xeigtsts.mak" CFG="xeigtsts - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "xeigtsts - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "xeigtsts - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "xeigtsts - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Eig_Release"
# PROP Intermediate_Dir "Eig_Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 Release\tmglib.lib ..\Release\clapack.lib ..\blas\Release\blas.lib ..\f2clibs\Release\libF77.lib ..\f2clibs\Release\libI77.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386

!ELSEIF  "$(CFG)" == "xeigtsts - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "xeigtsts___Win32_Debug"
# PROP BASE Intermediate_Dir "xeigtsts___Win32_Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Eig_Debug"
# PROP Intermediate_Dir "Eig_Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 Debug\tmglib.lib ..\Debug\clapack.lib ..\blas\Debug\blas.lib ..\f2clibs\Debug\libF77.lib ..\f2clibs\Debug\libI77.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "xeigtsts - Win32 Release"
# Name "xeigtsts - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Group "aeigtst"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\Eig\alahdg.c
# End Source File
# Begin Source File

SOURCE=.\Eig\alareq.c
# End Source File
# Begin Source File

SOURCE=.\Eig\alasum.c
# End Source File
# Begin Source File

SOURCE=.\Eig\alasvm.c
# End Source File
# Begin Source File

SOURCE=.\Eig\chkxer.c
# End Source File
# Begin Source File

SOURCE=.\Eig\ilaenv.c
# End Source File
# Begin Source File

SOURCE=.\EIG\sdrges.c
# End Source File
# Begin Source File

SOURCE=.\EIG\sdrgev.c
# End Source File
# Begin Source File

SOURCE=.\EIG\sdrgsx.c
# End Source File
# Begin Source File

SOURCE=.\EIG\sdrgvx.c
# End Source File
# Begin Source File

SOURCE=.\EIG\sget54.c
# End Source File
# Begin Source File

SOURCE=.\MATGEN\slakf2.c
# End Source File
# Begin Source File

SOURCE=.\MATGEN\slatm5.c
# End Source File
# Begin Source File

SOURCE=.\MATGEN\slatm6.c
# End Source File
# Begin Source File

SOURCE=.\EIG\slctes.c
# End Source File
# Begin Source File

SOURCE=.\EIG\slctsx.c
# End Source File
# Begin Source File

SOURCE=.\Eig\xerbla.c
# End Source File
# Begin Source File

SOURCE=.\Eig\xlaenv.c
# End Source File
# End Group
# Begin Group "seigtst"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\Eig\sbdt01.c
# End Source File
# Begin Source File

SOURCE=.\Eig\sbdt02.c
# End Source File
# Begin Source File

SOURCE=.\Eig\sbdt03.c
# End Source File
# Begin Source File

SOURCE=.\Eig\schkbb.c
# End Source File
# Begin Source File

SOURCE=.\Eig\schkbd.c
# End Source File
# Begin Source File

SOURCE=.\Eig\schkbk.c
# End Source File
# Begin Source File

SOURCE=.\Eig\schkbl.c
# End Source File
# Begin Source File

SOURCE=.\Eig\schkec.c
# End Source File
# Begin Source File

SOURCE=.\Eig\schkee.c
# End Source File
# Begin Source File

SOURCE=.\Eig\schkgg.c
# End Source File
# Begin Source File

SOURCE=.\Eig\schkgk.c
# End Source File
# Begin Source File

SOURCE=.\Eig\schkgl.c
# End Source File
# Begin Source File

SOURCE=.\Eig\schkhs.c
# End Source File
# Begin Source File

SOURCE=.\Eig\schksb.c
# End Source File
# Begin Source File

SOURCE=.\Eig\schkst.c
# End Source File
# Begin Source File

SOURCE=.\Eig\sckglm.c
# End Source File
# Begin Source File

SOURCE=.\Eig\sckgqr.c
# End Source File
# Begin Source File

SOURCE=.\Eig\sckgsv.c
# End Source File
# Begin Source File

SOURCE=.\Eig\scklse.c
# End Source File
# Begin Source File

SOURCE=.\Eig\sdrvbd.c
# End Source File
# Begin Source File

SOURCE=.\Eig\sdrves.c
# End Source File
# Begin Source File

SOURCE=.\Eig\sdrvev.c
# End Source File
# Begin Source File

SOURCE=.\Eig\sdrvgg.c
# End Source File
# Begin Source File

SOURCE=.\Eig\sdrvsg.c
# End Source File
# Begin Source File

SOURCE=.\Eig\sdrvst.c
# End Source File
# Begin Source File

SOURCE=.\Eig\sdrvsx.c
# End Source File
# Begin Source File

SOURCE=.\Eig\sdrvvx.c
# End Source File
# Begin Source File

SOURCE=.\Eig\serrbd.c
# End Source File
# Begin Source File

SOURCE=.\Eig\serrec.c
# End Source File
# Begin Source File

SOURCE=.\Eig\serred.c
# End Source File
# Begin Source File

SOURCE=.\Eig\serrgg.c
# End Source File
# Begin Source File

SOURCE=.\Eig\serrhs.c
# End Source File
# Begin Source File

SOURCE=.\Eig\serrst.c
# End Source File
# Begin Source File

SOURCE=.\Eig\sget02.c
# End Source File
# Begin Source File

SOURCE=.\Eig\sget10.c
# End Source File
# Begin Source File

SOURCE=.\Eig\sget22.c
# End Source File
# Begin Source File

SOURCE=.\Eig\sget23.c
# End Source File
# Begin Source File

SOURCE=.\Eig\sget24.c
# End Source File
# Begin Source File

SOURCE=.\Eig\sget31.c
# End Source File
# Begin Source File

SOURCE=.\Eig\sget32.c
# End Source File
# Begin Source File

SOURCE=.\Eig\sget33.c
# End Source File
# Begin Source File

SOURCE=.\Eig\sget34.c
# End Source File
# Begin Source File

SOURCE=.\Eig\sget35.c
# End Source File
# Begin Source File

SOURCE=.\Eig\sget36.c
# End Source File
# Begin Source File

SOURCE=.\Eig\sget37.c
# End Source File
# Begin Source File

SOURCE=.\Eig\sget38.c
# End Source File
# Begin Source File

SOURCE=.\Eig\sget39.c
# End Source File
# Begin Source File

SOURCE=.\Eig\sget51.c
# End Source File
# Begin Source File

SOURCE=.\Eig\sget52.c
# End Source File
# Begin Source File

SOURCE=.\Eig\sget53.c
# End Source File
# Begin Source File

SOURCE=.\Eig\sglmts.c
# End Source File
# Begin Source File

SOURCE=.\Eig\sgqrts.c
# End Source File
# Begin Source File

SOURCE=.\Eig\sgrqts.c
# End Source File
# Begin Source File

SOURCE=.\Eig\sgsvts.c
# End Source File
# Begin Source File

SOURCE=.\Eig\shst01.c
# End Source File
# Begin Source File

SOURCE=.\Eig\slarfy.c
# End Source File
# Begin Source File

SOURCE=.\Eig\slarhs.c
# End Source File
# Begin Source File

SOURCE=.\Eig\slatm4.c
# End Source File
# Begin Source File

SOURCE=.\Eig\slsets.c
# End Source File
# Begin Source File

SOURCE=.\Eig\sort01.c
# End Source File
# Begin Source File

SOURCE=.\Eig\sort03.c
# End Source File
# Begin Source File

SOURCE=.\Eig\ssbt21.c
# End Source File
# Begin Source File

SOURCE=.\Eig\ssgt01.c
# End Source File
# Begin Source File

SOURCE=.\Eig\sslect.c
# End Source File
# Begin Source File

SOURCE=.\Eig\sspt21.c
# End Source File
# Begin Source File

SOURCE=.\Eig\sstt21.c
# End Source File
# Begin Source File

SOURCE=.\Eig\sstt22.c
# End Source File
# Begin Source File

SOURCE=.\Eig\ssyt21.c
# End Source File
# Begin Source File

SOURCE=.\Eig\ssyt22.c
# End Source File
# End Group
# Begin Group "scgitst"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\Eig\slafts.c
# End Source File
# Begin Source File

SOURCE=.\Eig\slahd2.c
# End Source File
# Begin Source File

SOURCE=.\Eig\slasum.c
# End Source File
# Begin Source File

SOURCE=.\Eig\slatb9.c
# End Source File
# Begin Source File

SOURCE=.\Eig\sstech.c
# End Source File
# Begin Source File

SOURCE=.\Eig\sstect.c
# End Source File
# Begin Source File

SOURCE=.\Eig\ssvdch.c
# End Source File
# Begin Source File

SOURCE=.\Eig\ssvdct.c
# End Source File
# Begin Source File

SOURCE=.\Eig\ssxt1.c
# End Source File
# End Group
# End Group
# End Target
# End Project
