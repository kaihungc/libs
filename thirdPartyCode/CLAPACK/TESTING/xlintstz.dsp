# Microsoft Developer Studio Project File - Name="xlintstz" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=xlintstz - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "xlintstz.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "xlintstz.mak" CFG="xlintstz - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "xlintstz - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "xlintstz - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "xlintstz - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 Release\tmglib.lib ..\Release\clapack.lib ..\blas\Release\blas.lib ..\f2clibs\Release\libF77.lib ..\f2clibs\Release\libI77.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386

!ELSEIF  "$(CFG)" == "xlintstz - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 Debug\tmglib.lib ..\Debug\clapack.lib ..\blas\Debug\blas.lib ..\f2clibs\Debug\libF77.lib ..\f2clibs\Debug\libI77.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "xlintstz - Win32 Release"
# Name "xlintstz - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Group "alintst"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\Lin\aladhd.c
# End Source File
# Begin Source File

SOURCE=.\Lin\alaerh.c
# End Source File
# Begin Source File

SOURCE=.\Lin\alaesm.c
# End Source File
# Begin Source File

SOURCE=.\Lin\alahd.c
# End Source File
# Begin Source File

SOURCE=.\Lin\alareq.c
# End Source File
# Begin Source File

SOURCE=.\Lin\alasum.c
# End Source File
# Begin Source File

SOURCE=.\Lin\alasvm.c
# End Source File
# Begin Source File

SOURCE=.\Lin\chkxer.c
# End Source File
# Begin Source File

SOURCE=.\LIN\icopy.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ilaenv.c
# End Source File
# Begin Source File

SOURCE=.\Lin\xerbla.c
# End Source File
# Begin Source File

SOURCE=.\Lin\xlaenv.c
# End Source File
# End Group
# Begin Group "zlintst"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\Lin\dget06.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zchkaa.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zchkeq.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zchkgb.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zchkge.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zchkgt.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zchkhe.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zchkhp.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zchklq.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zchkpb.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zchkpo.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zchkpp.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zchkpt.c
# End Source File
# Begin Source File

SOURCE=.\LIN\zchkq3.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zchkql.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zchkqp.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zchkqr.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zchkrq.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zchksp.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zchksy.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zchktb.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zchktp.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zchktr.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zchktz.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zdrvgb.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zdrvge.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zdrvgt.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zdrvhe.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zdrvhp.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zdrvls.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zdrvpb.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zdrvpo.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zdrvpp.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zdrvpt.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zdrvsp.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zdrvsy.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zerrge.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zerrgt.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zerrhe.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zerrlq.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zerrls.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zerrpo.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zerrql.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zerrqp.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zerrqr.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zerrrq.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zerrsy.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zerrtr.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zerrtz.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zerrvx.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zgbt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zgbt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zgbt05.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zgelqs.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zgeqls.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zgeqrs.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zgerqs.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zget01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zget02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zget03.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zget04.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zget07.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zgtt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zgtt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zgtt05.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zhet01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zhpt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zlaipd.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zlaptm.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zlarhs.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zlatb4.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zlatsp.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zlatsy.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zlattb.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zlattp.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zlattr.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zlavhe.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zlavhp.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zlavsp.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zlavsy.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zlqt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zlqt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zlqt03.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zpbt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zpbt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zpbt05.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zpot01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zpot02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zpot03.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zpot05.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zppt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zppt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zppt03.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zppt05.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zptt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zptt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zptt05.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zqlt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zqlt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zqlt03.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zqpt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zqrt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zqrt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zqrt03.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zqrt11.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zqrt12.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zqrt13.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zqrt14.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zqrt15.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zqrt16.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zqrt17.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zrqt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zrqt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zrqt03.c
# End Source File
# Begin Source File

SOURCE=.\LIN\zrzt01.c
# End Source File
# Begin Source File

SOURCE=.\LIN\zrzt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zsbmv.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zspt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zspt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zspt03.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zsyt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zsyt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\zsyt03.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ztbt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ztbt03.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ztbt05.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ztbt06.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ztpt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ztpt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ztpt03.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ztpt05.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ztpt06.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ztrt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ztrt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ztrt03.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ztrt05.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ztrt06.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ztzt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ztzt02.c
# End Source File
# End Group
# Begin Source File

SOURCE=.\Lin\dlaord.c
# End Source File
# End Group
# End Target
# End Project
