Pre-compiled versions
---------------------

libgoto_core2-r1.26		Using default settings, win32 (cygwin gcc)
libgoto_core2-r1.26_x64.dll	Using default settings, x64 (PGI compiler)
libgoto_core2p-r1.26.dll	Using 'SMP = 1' and 'MAX_THREADS = 2', win32 (cygwin gcc)
libgoto_core2p-r1.26_x64.dll	Using 'SMP = 1' and 'MAX_THREADS = 4', x64 (PGI compiler)

Peter Kaufmann, 20-7-2009
