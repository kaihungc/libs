There may be a problem invoking 'lib' from 'make dll',
or 'libgoto_core2-r1.26.lib' may not get created for some reason.

In that case, simply open the Visual Studio Command Prompt,
go to the export directory and type 'lib /machine:x86 /def:libgoto_core2-r1.26.def'.
This should create the .lib file from the .def.


Peter Kaufmann, 24-2-2009
