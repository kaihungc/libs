/*********************************************************************/
/*                                                                   */
/*             Optimized BLAS libraries                              */
/*                     By Kazushige Goto <kgoto@tacc.utexas.edu>     */
/*                                                                   */
/* Copyright (c) The University of Texas, 2005. All rights reserved. */
/* UNIVERSITY EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES CONCERNING  */
/* THIS SOFTWARE AND DOCUMENTATION, INCLUDING ANY WARRANTIES OF      */
/* MERCHANTABILITY, FITNESS FOR ANY PARTICULAR PURPOSE,              */
/* NON-INFRINGEMENT AND WARRANTIES OF PERFORMANCE, AND ANY WARRANTY  */
/* THAT MIGHT OTHERWISE ARISE FROM COURSE OF DEALING OR USAGE OF     */
/* TRADE. NO WARRANTY IS EITHER EXPRESS OR IMPLIED WITH RESPECT TO   */
/* THE USE OF THE SOFTWARE OR DOCUMENTATION.                         */
/* Under no circumstances shall University be liable for incidental, */
/* special, indirect, direct or consequential damages or loss of     */
/* profits, interruption of business, or related expenses which may  */
/* arise from use of Software or Documentation, including but not    */
/* limited to those resulting from defects in Software and/or        */
/* Documentation, or loss or inaccuracy of data of any kind.         */
/*********************************************************************/

#define ASSEMBLER
#include "common.h"

#define N	ARG1	/* rdi */
#define X	ARG2	/* rsi */
#define INCX	ARG3	/* rdx */
#define Y	ARG4	/* rcx */
#ifndef WINDOWS_ABI
#define INCY	ARG5	/* r8  */
#else
#define INCY	%r10
#endif

#define PREFETCH	prefetch
#define PREFETCHSIZE	(16 * 7)

	PROLOGUE
	PROFCODE

#ifdef WINDOWS_ABI
	movq	40(%rsp), INCY
#endif

	SAVEREGISTERS

#ifdef F_INTERFACE
#ifndef USE64BITINT
	movslq	(N), N			# N
	movslq	(INCX),INCX		# INCX
	movslq	(INCY),INCY		# INCY
#else
	movq	(N), N			# N
	movq	(INCX),INCX		# INCX
	movq	(INCY),INCY		# INCY
#endif
#endif

	leaq	(, INCX, SIZE), INCX	
	leaq	(, INCY, SIZE), INCY	

	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	pxor	%xmm2, %xmm2
	pxor	%xmm3, %xmm3

	cmpq	$0, N
	jle	.L999

	cmpq	$SIZE, INCX
	jne	.L50
	cmpq	$SIZE, INCY
	jne	.L50

	cmpq	$3, N
	jle	.L27

	testq	$SIZE, Y
	je	.L05

	movss	0 * SIZE(X), %xmm0
	mulss	0 * SIZE(Y), %xmm0
	addq	$1 * SIZE, X
	addq	$1 * SIZE, Y
	decq	N
	ALIGN_2

.L05:
	testq	$2 * SIZE, Y
	je	.L10

	movsd	0 * SIZE(X), %xmm1
	movsd	0 * SIZE(Y), %xmm4

	mulps	%xmm4, %xmm1
	addq	$2 * SIZE, X
	addq	$2 * SIZE, Y
	subq	$2, N
	jle	.L999
	ALIGN_2

.L10:
	movq	N,    %rax
	andq	$-32, %rax
	jle	.L24

	leaq	(X, %rax, 4), X
	leaq	(Y, %rax, 4), Y
	negq	%rax

	movlps	  0 * SIZE(X, %rax, 4), %xmm4
	movhps	  2 * SIZE(X, %rax, 4), %xmm4
	movlps	  4 * SIZE(X, %rax, 4), %xmm5
	movhps	  6 * SIZE(X, %rax, 4), %xmm5
	movlps	  8 * SIZE(X, %rax, 4), %xmm6
	movhps	 10 * SIZE(X, %rax, 4), %xmm6
	movlps	 12 * SIZE(X, %rax, 4), %xmm7
	movhps	 14 * SIZE(X, %rax, 4), %xmm7

	movlps	 16 * SIZE(X, %rax, 4), %xmm8
	movhps	 18 * SIZE(X, %rax, 4), %xmm8
	movlps	 20 * SIZE(X, %rax, 4), %xmm9
	movhps	 22 * SIZE(X, %rax, 4), %xmm9
	movlps	 24 * SIZE(X, %rax, 4), %xmm10
	movhps	 26 * SIZE(X, %rax, 4), %xmm10
	movlps	 28 * SIZE(X, %rax, 4), %xmm11
	movhps	 30 * SIZE(X, %rax, 4), %xmm11

	mulps	  0 * SIZE(Y, %rax, 4), %xmm4
	mulps	  4 * SIZE(Y, %rax, 4), %xmm5
	mulps	  8 * SIZE(Y, %rax, 4), %xmm6
	mulps	 12 * SIZE(Y, %rax, 4), %xmm7

	subq	$-32, %rax
	jge	.L22
	ALIGN_3

.L21:
	mulps	-16 * SIZE(Y, %rax, 4), %xmm8
	addps	%xmm4,  %xmm0
	movlps	  0 * SIZE(X, %rax, 4), %xmm4
	movhps	  2 * SIZE(X, %rax, 4), %xmm4

	PREFETCH	(PREFETCHSIZE + 0) * SIZE(X, %rax, 4)

	mulps	-12 * SIZE(Y, %rax, 4), %xmm9
	addps	%xmm5,  %xmm1
	movlps	  4 * SIZE(X, %rax, 4), %xmm5
	movhps	  6 * SIZE(X, %rax, 4), %xmm5

	mulps	 -8 * SIZE(Y, %rax, 4), %xmm10
	addps	%xmm6,  %xmm2
	movlps	  8 * SIZE(X, %rax, 4), %xmm6
	movhps	 10 * SIZE(X, %rax, 4), %xmm6

	PREFETCH	(PREFETCHSIZE + 0) * SIZE(Y, %rax, 4)

	mulps	 -4 * SIZE(Y, %rax, 4), %xmm11
	addps	%xmm7,  %xmm3
	movlps	 12 * SIZE(X, %rax, 4), %xmm7
	movhps	 14 * SIZE(X, %rax, 4), %xmm7

	mulps	  0 * SIZE(Y, %rax, 4), %xmm4
	addps	%xmm8,  %xmm0
	movlps	 16 * SIZE(X, %rax, 4), %xmm8
	movhps	 18 * SIZE(X, %rax, 4), %xmm8

	PREFETCH	(PREFETCHSIZE + 16) * SIZE(X, %rax, 4)

	mulps	  4 * SIZE(Y, %rax, 4), %xmm5
	addps	%xmm9,  %xmm1
	movlps	 20 * SIZE(X, %rax, 4), %xmm9
	movhps	 22 * SIZE(X, %rax, 4), %xmm9

	mulps	  8 * SIZE(Y, %rax, 4), %xmm6
	addps	%xmm10, %xmm2
	movlps	 24 * SIZE(X, %rax, 4), %xmm10
	movhps	 26 * SIZE(X, %rax, 4), %xmm10

	PREFETCH	(PREFETCHSIZE + 16) * SIZE(Y, %rax, 4)

	mulps	 12 * SIZE(Y, %rax, 4), %xmm7
	addps	%xmm11, %xmm3
	movlps	 28 * SIZE(X, %rax, 4), %xmm11
	movhps	 30 * SIZE(X, %rax, 4), %xmm11

	subq	$-32, %rax
	jl,pt	.L21
	ALIGN_3

.L22:
	mulps	 -16 * SIZE(Y), %xmm8
	mulps	 -12 * SIZE(Y), %xmm9
	mulps	  -8 * SIZE(Y), %xmm10
	mulps	  -4 * SIZE(Y), %xmm11

	addps	%xmm4,  %xmm0
	addps	%xmm5,  %xmm1
	addps	%xmm6,  %xmm2
	addps	%xmm7,  %xmm3

	addps	%xmm8,  %xmm0
	addps	%xmm9,  %xmm1
	addps	%xmm10, %xmm2
	addps	%xmm11, %xmm3
	ALIGN_3

.L24:
	testq	$31, N
	je	.L999

	testq	$16, N
	jle	.L25

	movlps	 0 * SIZE(X), %xmm4
	movhps	 2 * SIZE(X), %xmm4
	movlps	 4 * SIZE(X), %xmm5
	movhps	 6 * SIZE(X), %xmm5
	movlps	 8 * SIZE(X), %xmm6
	movhps	10 * SIZE(X), %xmm6
	movlps	12 * SIZE(X), %xmm7
	movhps	14 * SIZE(X), %xmm7

	mulps	 0 * SIZE(Y), %xmm4
	mulps	 4 * SIZE(Y), %xmm5
	mulps	 8 * SIZE(Y), %xmm6
	mulps	12 * SIZE(Y), %xmm7

	addps	%xmm4,  %xmm0
	addps	%xmm5,  %xmm1
	addps	%xmm6,  %xmm2
	addps	%xmm7,  %xmm3

	addq	$16 * SIZE, X
	addq	$16 * SIZE, Y
	ALIGN_3

.L25:
	testq	$8, N
	jle	.L26

	movlps	 0 * SIZE(X), %xmm4
	movhps	 2 * SIZE(X), %xmm4
	movlps	 4 * SIZE(X), %xmm5
	movhps	 6 * SIZE(X), %xmm5

	mulps	 0 * SIZE(Y), %xmm4
	mulps	 4 * SIZE(Y), %xmm5

	addps	%xmm4,  %xmm0
	addps	%xmm5,  %xmm1

	addq	$8 * SIZE, X
	addq	$8 * SIZE, Y
	ALIGN_3

.L26:
	testq	$4, N
	jle	.L27

	movlps	 0 * SIZE(X), %xmm4
	movhps	 2 * SIZE(X), %xmm4

	mulps	 0 * SIZE(Y), %xmm4
	addps	%xmm4,  %xmm0

	addq	$4 * SIZE, X
	addq	$4 * SIZE, Y
	ALIGN_3

.L27:
	testq	$2, N
	jle	.L28

	movsd	 0 * SIZE(X), %xmm4
	movsd	 0 * SIZE(Y), %xmm8

	mulps	 %xmm8,  %xmm4
	addps	 %xmm4,  %xmm0

	addq	$2 * SIZE, X
	addq	$2 * SIZE, Y
	ALIGN_3

.L28:
	testq	$1, N
	jle	.L999

	movss	 (X), %xmm4
	mulss	 (Y), %xmm4

	addss	%xmm4,  %xmm0
	jmp	.L999
	ALIGN_3


.L50:
#ifdef F_INTERFACE
	testq	INCX, INCX
	jge	.L51

	movq	N, %rax
	decq	%rax
	imulq	INCX, %rax
	subq	%rax, X
	ALIGN_3

.L51:
	testq	INCY, INCY
	jge	.L52

	movq	N, %rax
	decq	%rax
	imulq	INCY, %rax
	subq	%rax, Y
	ALIGN_3
.L52:
#endif

	movq	N,  %rax
	sarq	$2, %rax
	jle	.L55
	ALIGN_3

.L53:
	movss	(X), %xmm4
	addq	INCX, X
	mulss	(Y), %xmm4
	addq	INCY, Y
	movss	(X), %xmm5
	addq	INCX, X
	mulss	(Y), %xmm5
	addq	INCY, Y
	movss	(X), %xmm6
	addq	INCX, X
	mulss	(Y), %xmm6
	addq	INCY, Y
	movss	(X), %xmm7
	addq	INCX, X
	mulss	(Y), %xmm7
	addq	INCY, Y

	addss	%xmm4, %xmm0
	addss	%xmm5, %xmm1
	addss	%xmm6, %xmm2
	addss	%xmm7, %xmm3

	decq	%rax
	jg	.L53
	ALIGN_3

.L55:
	movq	N, %rax
	andq	$3,   %rax
	jle	.L999
	ALIGN_3

.L56:
	movss	(X), %xmm4
	mulss	(Y), %xmm4
	addss	%xmm4, %xmm0

	addq	INCX, X
	addq	INCY, Y
	decq	%rax
	jg	.L56
	ALIGN_3

.L999:
	addps	%xmm1, %xmm0
	addps	%xmm3, %xmm2
	addps	%xmm2, %xmm0

#ifndef HAVE_SSE3
	movhlps	%xmm0, %xmm1
	addps	%xmm1, %xmm0
	
	movaps	%xmm0, %xmm1
	shufps  $1, %xmm0, %xmm0
	addss	 %xmm1, %xmm0
#else
	haddps	%xmm0, %xmm0
	haddps	%xmm0, %xmm0
#endif

#if !defined(DOUBLE) && defined(F_INTERFACE) && defined(NEED_F2CCONV)
	cvtss2sd	%xmm0, %xmm0
#endif

	RESTOREREGISTERS

	ret

	EPILOGUE
